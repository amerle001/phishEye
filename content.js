"use strict";

const getDivAlert = (evilLink) => {
  const sanitizedLink = `<a href="javascript:void(0)">${evilLink}</a>`
  const message = `<strong>Attention !</strong> L'extension "UPPA anti phishing" a détecté un lien malveillant dans ce message (${sanitizedLink}) et l'a désactivé"`;

  let div = document.createElement("div");
  div.style.padding = "15px";
  div.style.border = "1px solid #d6e9c6";
  div.style.borderRadius = "4px";
  div.style.color = "1px solid #d6e9c6";
  div.style.backgroundColor = "#dff0d8";
  div.innerHTML = message;

  return div;
};

(window.setInterval(() => {
  // Chaque mail de partage s'ouvre dans une iframe, qui est un DOM dans le DOM
  let iframe = document.getElementsByTagName("iframe");

  if (iframe.length) {
    // loop dans htmlcollection si multiple iframes (ex: dans une conversation)
    const keys = Object.keys(iframe);
    for (const k in keys) {
      const innerDoc =
        iframe[k].contentWindow.document || iframe[k].contentDocument;
      const links = innerDoc.getElementsByTagName("a");

      for (const link of links) {
        if (
          link.innerHTML.includes("Intranet") ||
          link.innerHTML.includes("intranet")
        ) {
          if (!link.href.match(/https?:\/\/([a-z0-9]+[.])*univ-pau[.]fr\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/)) {
            const div = getDivAlert(link.href);
            link.parentNode.prepend(div);
            console.log(`Lien malveillant supprimé : ${link.innerHTML}`);
            link.remove();
          }
        }
      }
    }
  }
}),
2000)();
