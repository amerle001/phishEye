console.log("Phish Eye est actif");

browser.browserAction.onClicked.addListener((tab) => {
  browser.browserAction.isEnabled({}).then((enabled) => {
    if (enabled) {
      browser.browserAction.disable(tab.id);
    }
    console.log(tab.url);
  });
});
