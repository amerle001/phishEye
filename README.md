# A propos
Extension Firefox destinée à protéger les utilisateurs de la messagerie UPPA de **certains** mails de phishing dans partage.
L'extension supprime le lien malveillant et le remplace par un cartouche vert explicatif.

## Attaque ciblée
A ce stade, l'extension ne cible que les mails de type "Validez votre intranet".

## Installation à des fins de tests
1. Télécharger la dernière release
2. Aller à l'adresse de gestion des extensions du navigateur 
   1. Firefox : about:addons
   2. Edge : edge://extensions/
   3. Brave: brave://extensions/
   4. Chrome : chrome://extensions/
3. puis glisser-déposer le fichier zip correspondant

